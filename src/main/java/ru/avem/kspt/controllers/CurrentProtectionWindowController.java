package ru.avem.kspt.controllers;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import ru.avem.kspt.communication.CommunicationModel;

import java.net.URISyntaxException;

import static ru.avem.kspt.Main.setTheme;

public class CurrentProtectionWindowController extends CurrentProtection {

    @FXML
    private AnchorPane root;

    @FXML
    public void initialize() throws URISyntaxException {
        setTheme(root);
        CommunicationModel model = CommunicationModel.getInstance();
        model.addObserver(this);
        model.initOwenPRforCurrentProtection();
    }
}
