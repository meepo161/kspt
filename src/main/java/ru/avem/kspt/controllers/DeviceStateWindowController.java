package ru.avem.kspt.controllers;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import ru.avem.kspt.communication.CommunicationModel;
import ru.avem.kspt.utils.Utils;

import java.net.URISyntaxException;

import static ru.avem.kspt.Main.setTheme;

public class DeviceStateWindowController extends DeviceState {
    @FXML
    private AnchorPane root;

    boolean flag = true;

    @FXML
    public void initialize() throws URISyntaxException {
        setTheme(root);
        CommunicationModel model = CommunicationModel.getInstance();
        model.addObserver(this);
        flag = true;

        new Thread(() -> {
            model.setNeedToReadAllDevices();

            while (flag) {
                model.setTwoAttemptsAllDevices();
                Utils.sleep(500);
            }

            model.finalizeAllDevices();
            model.deleteObservers();
        }).start();
    }
}