package ru.avem.kspt.controllers;

import javafx.stage.Stage;

public interface ExperimentController {
    void setDialogStage(Stage dialogStage);

    boolean isCanceled();
}
