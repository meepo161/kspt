package ru.avem.kspt.states.main;


public interface Statable {
    void toIdleState();

    void toWaitState();

    void toResultState();
}
