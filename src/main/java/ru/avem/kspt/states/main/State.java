package ru.avem.kspt.states.main;

public interface State {
    void toIdleState();

    void toWaitState();

    void toResultState();
}
